FROM python:3.10

ENV PIP_EXTRA_INDEX_URL=https://pypi.numerously.com/simple/

#RUN apt-get update && apt-get -y install gfortran
#RUN git clone https://github.com/modflowpy/pymake.git
#RUN python -m pip install ./pymake
#RUN python pymake/examples/make_mf6.py && cp mf6 usr/local/bin

COPY ./requirements.txt ./requirements.txt
RUN pip install -r requirements.txt
RUN get-modflow :flopy

COPY ./main.py ./main.py

CMD python main.py