from .client import client

def build_image(path_to_dockerfile, tag=None, push=False):

    print('BUILDING DOCKER IMAGE')
    image_obj, build_logs = client.images.build(path=path_to_dockerfile, tag=tag)

    for chunk in build_logs:
        if 'stream' in chunk:
            for line in chunk['stream'].splitlines():
                print(line)

    print('BUILD COMPLETED')

    if push:
        repo = tag.split(':')[0]
        tag_ = tag.split(':')[1]
        client.images.push(repository=repo, tag=tag_)

        print('IMAGE PUSHED as: ', tag)
    return image_obj.id
