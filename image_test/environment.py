import json, os
from .build import build_image
from .run import run_container


def read_numerous_environment(config_folder):

    with open(config_folder+'/.numerous.repository.json', 'r') as f:
        repo_config = json.load(f)


    project = repo_config['scenario'].split(':')[0]
    scenario = repo_config['scenario'].split(':')[1]
    job_id =  repo_config['remote'].split('/')[-2]
    server =  repo_config['remote'].split('/')[2]
    https = repo_config['remote'].split('/')[0]

    config = {
        'NUMEROUS_SCENARIO': scenario,
        'NUMEROUS_PROJECT': project,
        'JOB_ID': job_id,
        'NUMEROUS_API_REFRESH_TOKEN': repo_config['token'],
        'NUMEROUS_API_PORT': '443',
        'NUMEROUS_API_SERVER': server,
        'SECURE_CHANNEL': "True" if https else "False",
        'KUBERNETES_SERVICE_HOST': 'localhost'
    }

    return config

def read_set_numerous_environment(config_folder):
    environment = read_numerous_environment(config_folder)
    for k,v in environment.items():
        os.environ[k] = v
    return environment

def build_run_numerous_image(docker_context_folder=".", numerous_config_folder=".", project_scenario=None, tag=None, push=False):
    environment = read_numerous_environment(numerous_config_folder)
    if project_scenario:
        environment['NUMEROUS_PROJECT'] = project_scenario.split(':')[0]
        environment['NUMEROUS_SCENARIO'] = project_scenario.split(':')[1]


    image_id = build_image(docker_context_folder, tag=tag, push=push)

    run_container(image_id, environment)


