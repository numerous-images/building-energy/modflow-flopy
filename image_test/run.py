from .client import client

def run_container(image, environment):
    print('RUNNING DOCKER IMAGE')

    container_logs = client.containers.run(image, environment=environment, stderr=True, stdout=True)


    print(container_logs.decode("utf-8"))

    print('COMPLETED RUN OF DOCKER IMAGE')
