from numerous.image_tools.job import NumerousReportJob
from numerous.image_tools.app import run_job

import os, logging

class ModflowReport(NumerousReportJob):
    def __init__(self):
        super(ModflowReport, self).__init__()

    #Main callback from Report Job
    def add_report_content(self):
        self.app.client.set_scenario_progress("Running Modflow Simulation", "running", 0, force=True)

        from modflow_simulation import simulation

        result = simulation()

        self.report.add_info(
            title=f"Modflow-flopy report",
            filename="modflow_report",
            type_title="modflow",
            sub_title=f"",
            sub_sub_title=f"",

        )

        self.app.client.set_scenario_progress("Finished Modflow Simulation", "running", 100, force=True)


if __name__== "__main__":
    os.environ['LOG_LEVEL'] = "DEBUG"
    logging.basicConfig(level=logging.DEBUG)
    run_job(numerous_job=ModflowReport(), appname="modflow-flopy-report", model_folder=None)